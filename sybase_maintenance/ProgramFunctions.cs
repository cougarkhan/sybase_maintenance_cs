﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.ServiceProcess;
using System.Threading;
using System.Diagnostics;


namespace sybase_maintenance
{
    class ProgramFunctions
    {
        // Create new Log Functions object
        private LogFunctions lf = new LogFunctions();

        // Create new Timer Functions object
        private TimerFunctions tf1 = new TimerFunctions();
        private string time_result;

        // Define Warning Codes
        private string[] code = { "INFO", "WARNING", "ERROR" };

        public ServiceController getService(ProgramOptions po)
        {
            try
            {
                Console.WriteLine("{0,-12}{1,-12}Getting service {2} on {3}...", lf.format_logtime, code[0], po.sybase_service_name, po.sybase_server_name);
                tf1.set();
                ServiceController service = new ServiceController(po.sybase_service_name, po.sybase_server_name);
                time_result = tf1.get();
                if (service != null)
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} Service retrieved successfully from {3}. {4}", lf.format_logtime, code[0], service.DisplayName, service.MachineName, time_result);
                    return service;
                }
                else
                {
                    time_result = tf1.get();
                    Console.WriteLine("{0,-12}{1,-12}{2} Service could not be retrieved from {3}. {4}", lf.format_logtime, code[2], service.DisplayName, service.MachineName, time_result);
                    return service;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}{2} Service could not be retrieved from {3}.", lf.format_logtime, code[2], po.sybase_service_name, po.sybase_server_name);
                Console.WriteLine("{0,-12}{1,-12}{2}.", lf.format_logtime, code[2], e);
                return null;
            }

        }

        public bool stopService(ServiceController service)
        {
            service.Refresh();
            if (service.Status == ServiceControllerStatus.Running)
            {
                Console.WriteLine("{0,-12}{1,-12}Stopping service {2} on {3}...", lf.format_logtime, code[0], service.DisplayName, service.MachineName);
                try
                {
                    tf1.set();
                    service.Stop();
                    TimeSpan ts = new TimeSpan(0, 2, 0);
                    service.Refresh();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, ts);
                    time_result = tf1.get();
                    service.Refresh();
                    if (service.Status == ServiceControllerStatus.Stopped)
                    {
                        Console.WriteLine("{0,-12}{1,-12}{2} Stopped successfully. {3}", lf.format_logtime, code[0], service.DisplayName, time_result);
                        Console.WriteLine("{0,-12}{1,-12}{2} is {3}.", lf.format_logtime, code[0], service.DisplayName, service.Status);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("{0,-12}{1,-12}{2} could not be Stopped. {3}", lf.format_logtime, code[2], service.DisplayName, time_result);
                        Console.WriteLine("{0,-12}{1,-12}{2} is {3}.", lf.format_logtime, code[2], service.DisplayName, service.Status);
                        return false;
                    }
                }
                catch (Exception e)
                {
                    service.Refresh();
                    Console.WriteLine("{0,-12}{1,-12}{2} Did not respond to a Stop command in a timely fashion or an error occurred.  PLease check logs.", lf.format_logtime, code[2], service.DisplayName, service.Status);
                    Console.WriteLine("{0,-12}{1,-12}{2} is {3}.", lf.format_logtime, code[2], service.DisplayName, service.Status);
                    Console.WriteLine("{0,-12}{1,-12}{2}.", lf.format_logtime, code[2], e);
                    return false;
                }
            }
            else
            {
                Console.WriteLine("{0,-12}{1,-12}Service is not running.{2} on {3}...", lf.format_logtime, code[2], service.DisplayName, service.MachineName);
                return false;
            }
        }

        public bool startService(ServiceController service)
        {
            service.Refresh();
            if (service.Status == ServiceControllerStatus.Stopped)
            {
                Console.WriteLine("{0,-12}{1,-12}Starting service {2} on {3}...", lf.format_logtime, code[0], service.DisplayName, service.MachineName);
                try
                {
                    tf1.set();
                    service.Start();
                    TimeSpan ts = new TimeSpan(0, 2, 0);
                    service.Refresh();
                    service.WaitForStatus(ServiceControllerStatus.Running, ts);
                    time_result = tf1.get();
                    service.Refresh();
                    if (service.Status == ServiceControllerStatus.Running)
                    {
                        Console.WriteLine("{0,-12}{1,-12}{2} Started successfully. {3}", lf.format_logtime, code[0], service.DisplayName, time_result);
                        Console.WriteLine("{0,-12}{1,-12}{2} is {3}.", lf.format_logtime, code[0], service.DisplayName, service.Status);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("{0,-12}{1,-12}{2} could not be Started. {3}", lf.format_logtime, code[2], service.DisplayName, time_result);
                        Console.WriteLine("{0,-12}{1,-12}{2} is {3}.", lf.format_logtime, code[2], service.DisplayName, service.Status);
                        return false;
                    }
                }
                catch (Exception e)
                {
                    service.Refresh();
                    Console.WriteLine("{0,-12}{1,-12}{2} Did not respond to a Start command in a timely fashion or an error occurred.  PLease check logs.", lf.format_logtime, code[2], service.DisplayName, service.Status);
                    Console.WriteLine("{0,-12}{1,-12}{2} is {3}.", lf.format_logtime, code[2], service.DisplayName, service.Status);
                    Console.WriteLine("{0,-12}{1,-12}{2}.", lf.format_logtime, code[2], e);
                    return false;
                }
            }
            else
            {
                Console.WriteLine("{0,-12}{1,-12}Service is already running.{2} on {3}...", lf.format_logtime, code[2], service.DisplayName, service.MachineName);
                return false;
            }
        }

        public bool backupDatabase(ProgramOptions po)
        {
            string backup_filename = "ScanNetBackup_" + lf.format_scannet_backuptime + "*";
            if (!Directory.Exists(po.backup_working_folder))
            {
                try
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} Does not exist.  Attempting to create it.", lf.format_logtime, code[1], po.backup_working_folder);
                    Directory.CreateDirectory(po.backup_working_folder);
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} Could not be created.", lf.format_logtime, code[2], po.backup_working_folder);
                    Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e);
                    return false;
                }
            }

            if (Directory.Exists(po.backup_working_folder))
            {
                try
                {
                    Console.WriteLine("{0,-12}{1,-12}Begining Backup of {2}.", lf.format_logtime, code[0], po.database_name);
                    tf1.set();
                    string scannet_backup_process_arguments = "-dbpassword " + po.sybase_database_admin_password + " -destination " + po.backup_working_folder + " -" + po.database_name + " -nocompress -go -close";
                    Process scannet_backup_process = new Process();
                    scannet_backup_process.StartInfo.FileName = po.backup_exe;
                    scannet_backup_process.StartInfo.Arguments = scannet_backup_process_arguments;
                    scannet_backup_process.StartInfo.UseShellExecute = false;
                    scannet_backup_process.StartInfo.RedirectStandardInput = true;
                    scannet_backup_process.StartInfo.RedirectStandardOutput = true;
                    scannet_backup_process.StartInfo.CreateNoWindow = true;
                    Console.WriteLine("{0,-12}{1,-12}Created Scannet Backup Process {2}. ", lf.format_logtime, code[0], po.backup_exe);
                    Console.WriteLine("{0,-12}{1,-12}Launching Scannet Backup Process {2}. ", lf.format_logtime, code[0], po.backup_exe);
                    scannet_backup_process.Start();
                    scannet_backup_process.WaitForExit();
                    time_result = tf1.get();
                    if (scannet_backup_process.ExitCode == 0)
                    {
                        Console.WriteLine("{0,-12}{1,-12}Scannet Backup Process exited with code {2}. {3}", lf.format_logtime, code[0], scannet_backup_process.ExitCode, time_result);
                        DirectoryInfo[] bkl = (new DirectoryInfo(po.backup_working_folder)).GetDirectories(backup_filename, SearchOption.AllDirectories);
                        if (bkl.Length == 1)
                        {
                            Console.WriteLine("{0,-12}{1,-12}Found backup folder {2}.", lf.format_logtime, code[0], bkl[0].Name);
                            return true;
                        }
                        if (bkl.Length == 0)
                        {
                            Console.WriteLine("{0,-12}{1,-12}backupDatabase() Could not find backup folder.", lf.format_logtime, code[2]);
                            return false;
                        }
                        if (bkl.Length > 1)
                        {
                            Console.WriteLine("{0,-12}{1,-12}backupDatabase() Multiple backup folders found.", lf.format_logtime, code[2]);
                            return false;
                        }
                        return false;
                    }
                    else
                    {
                        Console.WriteLine("{0,-12}{1,-12}Scannet Backup Process exited with code {2}. {3}", lf.format_logtime, code[2], scannet_backup_process.ExitCode, time_result);
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0,-12}{1,-12}Backup of {2} Failed.", lf.format_logtime, code[2], po.database_name);
                    Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e);
                    return false;
                }
            }
            else
            {
                Console.WriteLine("{0,-12}{1,-12}{2} Not found.", lf.format_logtime, code[2], po.backup_working_folder);
                return false;
            }
        }

        public bool truncateDatabaseLogs(ProgramOptions po)
        {
            Console.WriteLine("{0,-12}{1,-12}Truncating logs files for {2}.", lf.format_logtime, code[0], po.database_name);

            try
            {
                string scannet_truncate_process_arguments = "-c uid=" + po.sybase_database_admin + ";pwd=" + po.sybase_database_admin_password + ";dsn=" + po.database_name + " -xo";
                Process scannet_truncate_process = new Process();
                scannet_truncate_process.StartInfo.FileName = po.truncate_exe;
                scannet_truncate_process.StartInfo.Arguments = scannet_truncate_process_arguments;
                scannet_truncate_process.StartInfo.UseShellExecute = false;
                scannet_truncate_process.StartInfo.RedirectStandardInput = true;
                scannet_truncate_process.StartInfo.RedirectStandardOutput = true;
                scannet_truncate_process.StartInfo.CreateNoWindow = true;
                Console.WriteLine("{0,-12}{1,-12}Created Scannet Database Truncate Process {2}. ", lf.format_logtime, code[0], po.truncate_exe);
                Console.WriteLine("{0,-12}{1,-12}Launching Scannet Database Truncate Process {2}. ", lf.format_logtime, code[0], po.truncate_exe);
                scannet_truncate_process.Start();
                scannet_truncate_process.WaitForExit();
                time_result = tf1.get();
                if (scannet_truncate_process.ExitCode == 0)
                {
                    Console.WriteLine("{0,-12}{1,-12}Scannet Database Truncate Process exited with code {2}. {3}", lf.format_logtime, code[0], scannet_truncate_process.ExitCode, time_result);
                    return true;
                }
                else
                {
                    Console.WriteLine("{0,-12}{1,-12}Scannet Database Truncate Process exited with code {2}. {3}", lf.format_logtime, code[2], scannet_truncate_process.ExitCode, time_result);
                    return false;
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Truncate of {2} Failed.", lf.format_logtime, code[2], po.database_name);
                Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e);
                return false;
            }
        }
    }
}
