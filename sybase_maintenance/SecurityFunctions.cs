﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Security.Principal;

namespace sybase_maintenance
{
    class SecurityFunctions
    {
        public static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }

        public static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }
            return plaintext;
        }

        public static void createEnryptedPasswordFile(string password, string password_file, string key_file, string IV_file)
        {
            if (!File.Exists(password_file))
            {
                FileStream fs1 = new FileStream(password_file, FileMode.OpenOrCreate, FileAccess.Write);
                fs1.Close();
            }
            if (!File.Exists(key_file))
            {
                FileStream fs2 = new FileStream(key_file, FileMode.OpenOrCreate, FileAccess.Write);
                fs2.Close();
            }
            if (!File.Exists(IV_file))
            {
                FileStream fs3 = new FileStream(IV_file, FileMode.OpenOrCreate, FileAccess.Write);
                fs3.Close();
            }

            if (File.Exists(password_file) && File.Exists(key_file) && File.Exists(IV_file))
            {
                byte[] encrypted_password;

                RijndaelManaged rm = new RijndaelManaged();
                rm.GenerateKey();
                rm.GenerateIV();
                
                encrypted_password = EncryptStringToBytes(password, rm.Key, rm.IV);

                File.WriteAllBytes(password_file, encrypted_password);
                File.WriteAllBytes(key_file, rm.Key);
                File.WriteAllBytes(IV_file, rm.IV);
            }
        }

        public static string readEncryptedFile(string encrypted_file, string key_file, string IV_file)
        {
            if (File.Exists(encrypted_file) && File.Exists(key_file) && File.Exists(IV_file))
            {
                byte[] encrypted_password = File.ReadAllBytes(encrypted_file);
                byte[] key = File.ReadAllBytes(key_file);
                byte[] IV = File.ReadAllBytes(IV_file);

                string password = DecryptStringFromBytes(encrypted_password, key, IV);

                return password;
            }
            else
            {
                //return null;
                throw new FileNotFoundException("One or more password files not found.");
            }
        }

        public static void test()
        {
            WindowsIdentity wi = new WindowsIdentity("sadmin@bussops.ubc.ca");
            WindowsImpersonationContext ctx = null;
            ctx = wi.Impersonate();

            try
            {
                File.Create(@"C:\temp\sadmin_newfile.txt");
            }
            catch (Exception e)
            {
            }
            finally
            {
                ctx.Undo();
            }
        }
    }
}
