﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using System.Collections;
using System.Xml.Linq;
using WinSCP;


namespace sybase_maintenance
{
    class MaintFunctions
    {
        // Create new Log Functions object
        private LogFunctions lf = new LogFunctions();

        // Create new Timer Functions object
        private TimerFunctions tf1 = new TimerFunctions();
        private string time_result;

        // Define Warning Codes
        private string[] code = { "INFO", "WARNING", "ERROR" };

        private List<FileInfo> getBackupFiles(ProgramOptions po)
        {
            try
            {
                List<FileInfo> sf = new List<FileInfo>();

                string folder_mask = "ScanNetBackup_" + lf.format_scannet_backuptime + "*";

                string[] backup_folder = Directory.GetDirectories(po.backup_working_folder, folder_mask, SearchOption.AllDirectories);

                if (backup_folder.Length == 1)
                {
                    if (Directory.Exists(backup_folder[0]))
                    {
                        string[] files = Directory.GetFiles(backup_folder[0], "*", SearchOption.AllDirectories);

                        foreach (XElement include_file in po.backup_include_files)
                        {
                            foreach (string file in files)
                            {
                                if (file.Contains(include_file.Value))
                                {
                                    Console.WriteLine("{0,-12}{1,-12}Found File {2}.  Adding to Queue.", lf.format_logtime, code[0], file);
                                    sf.Add(new FileInfo(file));
                                }
                            }

                        }

                    }
                }
                else
                {
                    Console.WriteLine("{0,-12}{1,-12}Directory {2} Not Found.", lf.format_logtime, code[2], folder_mask);
                }

                return sf;
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Get Backup Files Failure.", lf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12} {2}.", lf.format_logtime, code[2], e);
                return null;
            }
        }

        public bool cleanFiles(string path_to_folder, int age_days, bool force)
        {
            try
            {
                // If the path_to_folder does not exist then the function fails
                if (Directory.Exists(path_to_folder) == false)
                {
                    Console.WriteLine("{0,-12}{1,-12}The folder {2} does not exist!", lf.format_logtime, code[2], path_to_folder);
                    return false;
                }
                else
                {
                    //Create a DirectoryInfo object of the directory to clean up
                    string[] file_list = Directory.GetFiles(path_to_folder);
                    string[] dir_list = Directory.GetDirectories(path_to_folder);

                    // File age Limit variable.  Determines how far back files will be kept
                    DateTime file_age_limit = DateTime.Now;
                    
                    int files_removed_count = 0;

                    // Measure results
                    TimerFunctions tf2 = new TimerFunctions();
                    tf2.set();

                    foreach (string item in dir_list)
                    {
                        DirectoryInfo obj = new DirectoryInfo(item);
                        
                        if (obj.Exists == true)
                        {
                            if ((file_age_limit - obj.CreationTime).TotalDays > age_days || force == true)
                            {
                                try
                                {
                                    tf1.set();
                                    obj.Delete(true);
                                    obj.Refresh();
                                    time_result = tf1.get();
                                    if (obj.Exists == false)
                                    {
                                        Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Success. {3}", lf.format_logtime, code[0], obj.FullName, time_result);
                                        files_removed_count++;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Failure. {3}", lf.format_logtime, code[2], obj.FullName, time_result);
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Failure.", lf.format_logtime, code[2], obj.FullName);
                                    Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e);
                                }
                            }
                        }
                    }

                    foreach (string item in file_list)
                    {
                        FileInfo obj = new FileInfo(item);

                        if (obj.Exists == true)
                        {
                            if ((file_age_limit - obj.CreationTime).TotalDays > age_days || force == true)
                            {
                                try
                                {
                                    tf1.set();
                                    obj.Delete();
                                    obj.Refresh();
                                    time_result = tf1.get();
                                    if (obj.Exists == false)
                                    {
                                        Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Success. {3}", lf.format_logtime, code[0], obj.FullName, time_result);
                                        files_removed_count++;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Failure. {3}", lf.format_logtime, code[2], obj.FullName, time_result);
                                    }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Failure.", lf.format_logtime, code[2], obj.FullName);
                                    Console.WriteLine("{0,-12}{1,-12}{2}", lf.format_logtime, code[2], e);
                                }
                            }
                        }
                    }

                    time_result = tf2.get();
                    Console.WriteLine("{0,-12}{1,-12}Completed Cleaning Files in {2}.", lf.format_logtime, code[0], time_result);

                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Clean Files Failure.", lf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12} {2}.", lf.format_logtime, code[2], e);
                return false;
            }
        }

        public bool moveFilesCIFS(FileInfo source_file, string destination_folder, List<string> include_files)
        {
            try
            {
                string time_result;
                string destination_file = destination_folder + "\\" + source_file.Name;

                destination_file = destination_folder + "\\" + source_file.Name;
                Console.WriteLine("{0,-12}{1,-12}Copying {2} -> {3}", lf.format_logtime, code[0], source_file.Name, destination_file);
                tf1.set();
                source_file.CopyTo(destination_file);
                time_result = tf1.get();
                Console.WriteLine("{0,-12}{1,-12}File Copy Completed. {2}", lf.format_logtime, code[0], time_result);

                FileInfo new_file = new FileInfo(destination_file);
                if (new_file.Exists)
                {
                    Console.WriteLine("{0,-12}{1,-12}Checking if Destination File Exists...Success.", lf.format_logtime, code[0]);
                    return true;
                }
                else
                {
                    Console.WriteLine("{0,-12}{1,-12}Checking if Destination File Exists...Failure.", lf.format_logtime, code[2]);
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Transfer Failure.", lf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12} {2}.", lf.format_logtime, code[2], e);
                return false;
            }
        }

        public bool moveFilesCIFS(ProgramOptions po, FileInfo source_file)
        {
            try
            {
                string time_result;
                string destination_file = po.backup_destination + "\\" + source_file.Name;

                destination_file = po.backup_destination + "\\" + source_file.Name;
                Console.WriteLine("{0,-12}{1,-12}Copying {2} -> {3}", lf.format_logtime, code[0], source_file.Name, destination_file);
                tf1.set();
                source_file.CopyTo(destination_file);
                time_result = tf1.get();
                Console.WriteLine("{0,-12}{1,-12}File Copy Completed. {2}", lf.format_logtime, code[0], time_result);

                FileInfo new_file = new FileInfo(destination_file);
                if (new_file.Exists)
                {
                    Console.WriteLine("{0,-12}{1,-12}Checking if Destination File Exists...Success.", lf.format_logtime, code[0]);
                    return true;
                }
                else
                {
                    Console.WriteLine("{0,-12}{1,-12}Checking if Destination File Exists...Failure.", lf.format_logtime, code[2]);
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Transfer Failure.", lf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12} {2}.", lf.format_logtime, code[2], e);
                return false;
            }
        }

        //public bool moveFilesSFTP(ProgramOptions po)
        //{
        //    string time_result;

        //    List<FileInfo> sf = getBackupFiles(po);
            
        //    List<string> winscp_script_commands = new List<string>();

        //    Console.WriteLine("{0,-12}{1,-12}Generating SFTP Commands...Success", lf.format_logtime, code[0]);

        //    winscp_script_commands.Add("option batch on");
        //    winscp_script_commands.Add("option confirm off");
        //    winscp_script_commands.Add("open sftp://" + po.winscp_username + "@" + po.winscp_hostname + " -certificate=\"" + po.winscp_server_finger_print + "\"");
        //    winscp_script_commands.Add("option transfer binary");
        //    foreach (FileInfo item in sf)
        //    {
        //        winscp_script_commands.Add("put " + item.FullName);
        //    }
        //    winscp_script_commands.Add("close");
        //    winscp_script_commands.Add("exit");

        //    string winscp_command_args = "/console /log=C:\\DRS\\Logs\\bak_scan_backup-" + lf.format_datetime + ".log /privatekey=" + po.winscp_certificate_file;

        //    Console.WriteLine("{0,-12}{1,-12}Initiating SFTP Transfer to {2}...", lf.format_logtime, code[0], po.winscp_hostname);

        //    // Run hidden WinSCP process
        //    Process winscp = new Process();
        //    winscp.StartInfo.FileName = po.winscp_exe;
        //    winscp.StartInfo.Arguments = winscp_command_args;
        //    winscp.StartInfo.UseShellExecute = false;
        //    winscp.StartInfo.RedirectStandardInput = true;
        //    winscp.StartInfo.RedirectStandardOutput = true;
        //    winscp.StartInfo.CreateNoWindow = true;
        //    winscp.Start();

        //    // Feed in the scripting commands
        //    foreach (string command in winscp_script_commands)
        //    {
        //        winscp.StandardInput.WriteLine(command);
        //        //Console.WriteLine(command);
        //    }
        //    winscp.StandardInput.Close();

        //    // Collect all output (not used in this example)
        //    string output = winscp.StandardOutput.ReadToEnd();
 
        //    // Wait until WinSCP finishes
        //    winscp.WaitForExit();
        //    time_result = tf1.get();

        //    // Output WinSCP standard output to log
        //    string[] o = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
        //    foreach (var line in o)
        //    {
        //        Console.WriteLine("{0,-12}{1,-12}" + line, lf.format_logtime, code[0]);
        //    }

        //    // Report status
        //    if (winscp.ExitCode != 0)
        //    {
        //        Console.WriteLine("{0,-12}{1,-12}SFTP Transfer Result...Failure", lf.format_logtime, code[2], po.winscp_hostname);
        //        cleanFiles(po.backup_working_folder, 0);
        //        return true;
        //    }
        //    else
        //    {
        //        Console.WriteLine("{0,-12}{1,-12}SFTP Transfer Result...Success", lf.format_logtime, code[0]);
        //        cleanFiles(po.backup_working_folder, 0);
        //        return false;
        //    }
                       
        //}

        public bool moveFilesWinSCP(ProgramOptions po)
        {
            try
            {
                SessionOptions so = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = po.winscp_hostname,
                    UserName = po.winscp_username,
                    Password = "",
                    SshPrivateKeyPath = po.winscp_certificate_file,
                    SshHostKeyFingerprint = po.winscp_server_finger_print,
                };

                using (Session winscp_session = new Session())
                {
                    Console.WriteLine("{0,-12}{1,-12}Initiating SFTP Transfer to {2}...", lf.format_logtime, code[0], po.winscp_hostname);
                    winscp_session.Open(so);

                    TransferOptions transfer_options = new TransferOptions();
                    transfer_options.TransferMode = TransferMode.Binary;

                    List<FileInfo> sf = getBackupFiles(po);

                    TransferOperationResult transfer_result;

                    foreach (FileInfo item in sf)
                    {
                        Console.WriteLine("{0,-12}{1,-12}Uploading file {2}...", lf.format_logtime, code[0], item.FullName);
                        transfer_result = winscp_session.PutFiles(item.FullName, "/", false, transfer_options);

                        if (transfer_result.IsSuccess == false)
                        {
                            Console.WriteLine("{0,-12}{1,-12}Upload Failure of {2}.", lf.format_logtime, code[2], item.Name);
                            transfer_result.Check();
                        }

                        foreach (TransferEventArgs transfer in transfer_result.Transfers)
                        {
                            Console.WriteLine("{0,-12}{1,-12}Upload of {2} succeeded...", lf.format_logtime, code[0], transfer.FileName);
                        }
                    }
                    cleanFiles(po.backup_working_folder, 0, true);
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Upload Failure.", lf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12} {2}.", lf.format_logtime, code[2], e);
                return false;
            }
        }

        public FileInfo compressFiles(ProgramOptions po)
        {
            try
            {
                string backup_filename = "ScanNetBackup_" + lf.format_scannet_backuptime + "*";
                               
                DirectoryInfo[] folder = (new DirectoryInfo(po.backup_working_folder)).GetDirectories(backup_filename, SearchOption.AllDirectories);
                if (folder.Length == 1)
                {
                    Console.WriteLine("{0,-12}{1,-12}Found backup folder {2}.  Setting as source.", lf.format_logtime, code[0], folder[0].Name);
                }
                if (folder.Length == 0)
                {
                    Console.WriteLine("{0,-12}{1,-12}compressFile() Could not find backup folder.", lf.format_logtime, code[2]);
                    return null;
                }
                if (folder.Length > 1)
                {
                    Console.WriteLine("{0,-12}{1,-12}compressFile() Multiple backup folders found.", lf.format_logtime, code[2]);
                    return null;
                }

                string target_file = po.backup_working_folder + "\\" + folder[0].Name + ".7z";
                string seven_zip_arguments = "a " + target_file + " " + folder[0].FullName + " -mx=3 -m0=lzma2:d26 -mmt=1";

                Console.WriteLine("{0,-12}{1,-12}Compressing {2} to {3}.", lf.format_logtime, code[0], folder[0].FullName, target_file);
                tf1.set();
                Process seven_zip = new Process();
                seven_zip.StartInfo.FileName = po.seven_zip_exe;
                seven_zip.StartInfo.Arguments = seven_zip_arguments;
                seven_zip.StartInfo.UseShellExecute = false;
                seven_zip.StartInfo.RedirectStandardInput = true;
                seven_zip.StartInfo.RedirectStandardOutput = true;
                seven_zip.StartInfo.CreateNoWindow = true;
                seven_zip.Start();
                seven_zip.WaitForExit();
                time_result = tf1.get();

                string output = seven_zip.StandardOutput.ReadToEnd();

                string[] o = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                foreach (var line in o)
                {
                    Console.WriteLine("{0,-12}{1,-12}" + line, lf.format_logtime, code[0]);
                }
                Console.WriteLine("{0,-12}{1,-12}7Zip exited with code {2}. {3}", lf.format_logtime, code[0], seven_zip.ExitCode, time_result);
                Console.WriteLine("{0,-12}{1,-12}Completed Compressing {2}.", lf.format_logtime, code[0], target_file);

                if (File.Exists(target_file))
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} Found.", lf.format_logtime, code[0], target_file);
                    return (new FileInfo(target_file));
                }
                else
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} Not Found.", lf.format_logtime, code[2], target_file);
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Compress Files Failure.", lf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12} {2}.", lf.format_logtime, code[2], e);
                return null;
            }
        }
    }
}
