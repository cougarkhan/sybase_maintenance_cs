﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Net.Mail;

namespace sybase_maintenance
{
    class ProgramOptions : SecurityFunctions
    {
        public string server_name;
        public IEnumerable<XElement> backup_include_files;
        public string backup_destination;
        public string backup_exe;
        public string truncate_exe;
        public string backup_working_folder;
        public string seven_zip_exe;
        public string password_file;
        public string key_file;
        public string IV_file;
        public string database_name;
        public int file_age;
        public bool compress_files;
        public bool move_files_cifs;
        public bool move_files_sftp;
        public bool clean_up_files;
        public bool backup_files;
        public bool truncate_files;
        public bool sybase_stop_remote_service;
        public bool log_to_file;
        public string sybase_service_name;
        public string sybase_server_name;
        public string sybase_database_admin;
        public string sybase_database_admin_password;
        public string winscp_hostname;
        public string winscp_username;
        public string winscp_certificate_file;
        public string winscp_cert_finger_print;
        public string winscp_server_finger_print;
        private bool failure = false;

        public ProgramOptions()
        {
        }

        public ProgramOptions(string path_to_config_file)
        {
            string hostname = Environment.GetEnvironmentVariable("computername");
            string file_path = null;

            if (File.Exists(path_to_config_file))
            {
                file_path = path_to_config_file;
            }
            else if (Directory.Exists(path_to_config_file))
            {
                string temp_file = path_to_config_file + "\\" + hostname + ".xml";
                file_path = temp_file;
            }
            else
            {
                file_path = null;
                failure = true;
            }

            if (file_path != null)
            {
                XDocument xdoc = XDocument.Load(file_path);

                server_name = Convert.ToString(xdoc.Descendants("server").Elements("server_name").ElementAt(0).Value);
                backup_include_files = xdoc.Descendants("backup").Elements("include_file");
                backup_destination = Convert.ToString(xdoc.Descendants("backup").Elements("destination").ElementAt(0).Value);
                backup_exe = Convert.ToString(xdoc.Descendants("options").Elements("backup_exe").ElementAt(0).Value);
                truncate_exe = Convert.ToString(xdoc.Descendants("options").Elements("truncate_exe").ElementAt(0).Value);
                backup_working_folder = Convert.ToString(xdoc.Descendants("options").Elements("working_folder").ElementAt(0).Value);
                seven_zip_exe = Convert.ToString(xdoc.Descendants("options").Elements("seven_zip").ElementAt(0).Value);
                password_file = Convert.ToString(xdoc.Descendants("options").Elements("pw_file").ElementAt(0).Value);
                key_file = Convert.ToString(xdoc.Descendants("options").Elements("key_file").ElementAt(0).Value);
                IV_file = Convert.ToString(xdoc.Descendants("options").Elements("IV_file").ElementAt(0).Value);
                database_name = Convert.ToString(xdoc.Descendants("backup").Elements("db_name").ElementAt(0).Value);
                file_age = Convert.ToInt32(xdoc.Descendants("options").Elements("file_age").ElementAt(0).Value);
                compress_files = Convert.ToBoolean(xdoc.Descendants("backup").Elements("compress").ElementAt(0).Value);
                move_files_cifs = Convert.ToBoolean(xdoc.Descendants("backup").Elements("move_cifs").ElementAt(0).Value);
                move_files_sftp = Convert.ToBoolean(xdoc.Descendants("backup").Elements("move_sftp").ElementAt(0).Value);
                clean_up_files = Convert.ToBoolean(xdoc.Descendants("backup").Elements("clean_files").ElementAt(0).Value);
                backup_files = Convert.ToBoolean(xdoc.Descendants("backup").Elements("backup_files").ElementAt(0).Value);
                truncate_files = Convert.ToBoolean(xdoc.Descendants("backup").Elements("truncate_files").ElementAt(0).Value);
                log_to_file = Convert.ToBoolean(xdoc.Descendants("options").Elements("log_to_file").ElementAt(0).Value);
                sybase_stop_remote_service = Convert.ToBoolean(xdoc.Descendants("backup").Elements("stop_service").ElementAt(0).Value);
                sybase_service_name = Convert.ToString(xdoc.Descendants("backup").Elements("service_name").ElementAt(0).Value);
                sybase_server_name = Convert.ToString(xdoc.Descendants("backup").Elements("service_machine").ElementAt(0).Value);
                sybase_database_admin = Convert.ToString(xdoc.Descendants("backup").Elements("admin").ElementAt(0).Value);
                sybase_database_admin_password = readEncryptedFile(password_file, key_file, IV_file);
                winscp_hostname = Convert.ToString(xdoc.Descendants("sftp").Elements("hostname").ElementAt(0).Value);
                winscp_username = Convert.ToString(xdoc.Descendants("sftp").Elements("username").ElementAt(0).Value);
                winscp_certificate_file = Convert.ToString(xdoc.Descendants("sftp").Elements("cert_file").ElementAt(0).Value);
                winscp_server_finger_print = Convert.ToString(xdoc.Descendants("sftp").Elements("server_finger_print").ElementAt(0).Value);
                winscp_cert_finger_print = Convert.ToString(xdoc.Descendants("sftp").Elements("cert_finger_print").ElementAt(0).Value);
            }
        }
    }

    class MailOptions
    {
        public string sender;
        public IEnumerable<XElement> recipients;
        public string smtp_server;
        private bool failure = false;

        public MailOptions()
        {
        }

        public MailOptions(string path_to_config_file)
        {
            string hostname = Environment.GetEnvironmentVariable("computername");
            string file_path = null;

            if (File.Exists(path_to_config_file))
            {
                file_path = path_to_config_file;
            }
            else if (Directory.Exists(path_to_config_file))
            {
                string temp_file = path_to_config_file + "\\" + hostname + ".xml";
                file_path = temp_file;
            }
            else
            {
                file_path = null;
                failure = true;
            }

            if (file_path != null)
            {
                XDocument xdoc = XDocument.Load(file_path);

                sender = Convert.ToString(xdoc.Descendants("email").Elements("sender").ElementAt(0).Value);
                recipients = xdoc.Descendants("email").Elements("recipients");
                smtp_server = Convert.ToString(xdoc.Descendants("email").Elements("smtpserver").ElementAt(0).Value);
            }
        }

        public void sendLogEmail(string path_to_log_file, bool final_result)
        {
            string hostname = Environment.GetEnvironmentVariable("computername");
            MailMessage message = new MailMessage();
            SmtpClient smtp_client = new SmtpClient(smtp_server);

            foreach (XElement recipient in recipients)
            {
                message.To.Add(recipient.Value);
            }

            if (final_result == true)
            {
                message.Subject = hostname + " PASSED! Database Maintenance Tasks";
            }
            if (final_result == false)
            {
                message.Subject = hostname + " FAILED! Database Maintenance Tasks";
            }

            message.From = new MailAddress(sender);
            message.Body = File.ReadAllText(path_to_log_file);

            smtp_client.Send(message);
        }
    }
}
