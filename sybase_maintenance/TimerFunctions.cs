﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace sybase_maintenance
{
    class TimerFunctions
    {
        private Stopwatch sw;
        public TimeSpan ts;

        public TimerFunctions()
        {
            sw = new Stopwatch();
        }

        public void set()
        {
            //sw.Restart();
            sw.Reset();
            sw.Start();
        }

        public string get()
        {
            sw.Stop();
            string elapsed_time;
            ts = sw.Elapsed;

            elapsed_time = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            return elapsed_time;
        }
    }
}
