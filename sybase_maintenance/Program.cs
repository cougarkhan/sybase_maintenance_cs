﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.ServiceProcess;
using System.Threading;

namespace sybase_maintenance
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string arg in args)
            {
                // Load settings
                ProgramOptions po = new ProgramOptions(arg);
                MailOptions mo = new MailOptions(@".\etc\emailconfig.xml");

                // Create Program Functions Class instance
                ProgramFunctions pf = new ProgramFunctions();

                // Create Log Functions Class instance
                LogFunctions lf = new LogFunctions();

                // Create Timer Functions Class instance
                TimerFunctions tf1 = new TimerFunctions();

                // Create Maintenance Functions Class instance
                MaintFunctions mf = new MaintFunctions();

                // Create Error Stack
                Hashtable es = new Hashtable();

                // Create new Streamwriter object to output to log file
                string hostname = Environment.GetEnvironmentVariable("computername");
                string path_to_log = @"C:\temp\" + hostname + "-" + lf.format_datetime + ".log";
                FileStream output_stream = new FileStream(path_to_log, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter log = new StreamWriter(output_stream);
                TextWriter old_output = Console.Out;

                if (po.log_to_file)
                {
                    Console.SetOut(log);
                }
                else
                {
                    //Console.WindowWidth = Console.WindowWidth / 2;
                    //Console.WindowHeight = Console.WindowHeight / 2;
                    Console.WindowWidth = 120;
                    Console.WindowHeight = 40;
                    Console.SetBufferSize(120, 9999);
                }

                // Misc Values
                bool backup_result = false;
                string[] code = { "INFO", "WARNING", "ERROR" };
                FileInfo compressed_file = null;

                Console.WriteLine("*************************************");
                Console.WriteLine("Begining the Super Wicked Awesome Backup Script!");
                Console.WriteLine();
                Console.WriteLine("Start time: {0}", lf.format_datetime);
                Console.WriteLine(@"Username: {0}\{1}", Environment.GetEnvironmentVariable("USERDOMAIN"), Environment.GetEnvironmentVariable("USERNAME"));
                Console.WriteLine("Machine: {0}", Environment.GetEnvironmentVariable("COMPUTERNAME"));
                Console.WriteLine("*************************************");
                Console.WriteLine();

                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Skipped Operations...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();

                if (po.clean_up_files == false) { Console.WriteLine("{0,-12}{1,-12}Skipping Clean up of Backup Files.", lf.format_logtime, code[0]); }
                if (po.compress_files == false) { Console.WriteLine("{0,-12}{1,-12}Skipping Compress of Backup Files.", lf.format_logtime, code[0]); }
                if (po.move_files_cifs == false) { Console.WriteLine("{0,-12}{1,-12}Skipping CIFS Move of Backup Files.", lf.format_logtime, code[0]); }
                if (po.move_files_sftp == false) { Console.WriteLine("{0,-12}{1,-12}Skipping SFTP Move of Backup Files.", lf.format_logtime, code[0]); }
                if (po.backup_files == false) { Console.WriteLine("{0,-12}{1,-12}Skipping Backup of Database Files.", lf.format_logtime, code[0]); }
                if (po.truncate_files == false) { Console.WriteLine("{0,-12}{1,-12}Skipping Truncate of Database Log Files.", lf.format_logtime, code[0]); }

                if (po.clean_up_files == true)
                {
                    Console.WriteLine();
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Performing Cleanup Operations...");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine();

                    es.Add("Cleanup", mf.cleanFiles(po.backup_destination, po.file_age, false));
                }

                if (po.backup_files == true)
                {
                    Console.WriteLine();
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Performing Backup Operations...");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine();

                    backup_result = pf.backupDatabase(po);

                    es.Add("Backup", backup_result);
                }

                if (po.compress_files == true && backup_result == true)
                {
                    Console.WriteLine();
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Performing File Compression Operations...");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine();

                    compressed_file = mf.compressFiles(po);

                    if (compressed_file.Exists)
                    {
                        es.Add("Compress", true);
                    }
                    else
                    {
                        es.Add("Compress", false);
                    }

                }

                if (po.truncate_files == true && backup_result == true)
                {
                    Console.WriteLine();
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Performing Truncate Log Operations...");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine();

                    es.Add("Truncate", pf.truncateDatabaseLogs(po));
                }

                if (po.move_files_cifs == true && backup_result == true)
                {
                    Console.WriteLine();
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Performing CIFS File Transfer Operations...");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine();

                    es.Add("CIFS_Transfer", mf.moveFilesCIFS(po, compressed_file));
                }

                if (po.move_files_sftp == true && backup_result == true)
                {
                    Console.WriteLine();
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine("Performing SFTP File Transfer Operations...");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine();

                    ServiceController service = pf.getService(po);
                    pf.stopService(service);
                    Thread.Sleep(10000);
                    es.Add("SFTP_Transfer", mf.moveFilesWinSCP(po));
                    pf.startService(service);
                }

                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Script Cleanup...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();

                mf.cleanFiles(po.backup_working_folder, 0, true);

                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Listing Errors...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();

                int errors = 0;

                foreach (DictionaryEntry status in es)
                {
                    if ((bool)status.Value == false)
                    {
                        Console.WriteLine("{0,-12}{1,-12}ERROR! {2}", lf.format_logtime, code[2], (string)status.Key);
                        errors++;
                    }
                }

                Console.SetOut(old_output);
                log.Close();
                output_stream.Close();

                if (errors > 0)
                {
                    mo.sendLogEmail(path_to_log, false);
                }
                else
                {
                    mo.sendLogEmail(path_to_log, true);
                }
                Console.WriteLine("The End.");
                //Console.ReadLine();
            }
        }
    }
}
