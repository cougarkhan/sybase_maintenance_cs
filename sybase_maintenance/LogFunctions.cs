﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace sybase_maintenance
{
    class LogFunctions
    {
        private string f_dt;
        private string f_lt;
        private string f_sbt;

        public string format_datetime
        {
            get
            {
                return f_dt = (DateTime.Now.ToString("yyyyMMddHHmmss"));
            }
            set
            {
                f_dt = value;
            }
        }
        public string format_logtime
        {
            get
            {
                return f_lt = (DateTime.Now.ToString("HH:mm:ss"));
            }
            set
            {
                f_lt = value;
            }
        }
        public string format_scannet_backuptime
        {
            get
            {
                return f_sbt = (DateTime.Now.ToString("yyyyMMdd"));
            }
            set
            {
                f_sbt = value;
            }
        }
    }
}
